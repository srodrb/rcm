CC=gcc
CFLAGS=-std=c99 -Wall -Werror -g -O2
CINS=
CLIBS=
DEFINES=


objects = csr_graph.o linkedQueue.o rcm.o main.o

TARGET=main.exe


all:$(TARGET)

main.exe: $(objects)
	$(CC) $(CFLAGS) $(CINCS) $+ -o $@ $(CLIBS)

main.o: main.c
	$(CC) $(CFLAGS) $(CINCS) $+ -c

rcm.o: rcm.c
	$(CC) $(CFLAGS) $(CINCS) $+ -c

csr_graph.o:csr_graph.c
	$(CC) $(CFLAGS) $(CINCS) $+ -c

cqueue.o: cqueue.c
	$(CC) $(CFLAGS) $(CINCS) $+ -c

linkedQueue.o: linkedQueue.c
	$(CC) $(CFLAGS) $(CINCS) $+ -c

.PHONY:all clean run memcheck

clean:
	rm -rf *.o *.bin $(TARGET)

run:
	./$(TARGET)

memcheck:
	valgrind --leak-check=full ./$(TARGET)
