/*
 * =====================================================================================
 *
 *       Filename:  rcm.h
 *
 *    Description:  A naive implementation of the reverse Cuthill-McKee algorithm.
 *
 *        Version:  1.0
 *        Created:  25/05/2017 19:24:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Samuel Rodriguez Bernabeu, (srodrigu@bsc.es)
 *         Author:  Albert Coca Abello (acocaabe@bsc.es)
 *   Organization:  Barcelona Supercomputing Center
 *
 * =====================================================================================
 */

#ifndef _RCM_H_
#define _RCM_H_

	#define RCM_DEBUG    0     /* {0,1} disabled, enabled   */
	#define RCM_NO_SEED -1     /* see CuthillMcKee function */

	#include "linkedQueue.h" 
	#include "csr_graph.h"
	#include <limits.h>
	
	/*
	 * Computes the degree of each node of the graph.
	 * Then returns the node with the minimum degree.
	 * It Discards the nodes that already are in the Queue Q.
	 */
	GraphInt GetNodeWithLowestDegree(Graph *G, Queue *Q, char *isInR);
	
	/*
	 * Computes reverse Cuthill-Mkee ordering for the input graph.
	 *
	 * If StartingNodeId is >= 0, then it will be used as a seed for the
	 * algorithm, if a negative number or RCM_NO_SEED is provided then the
	 * algorithm will compute a feasible starting node.
	 *
	 * A note about heuristic for computing a initial seed: at the moment
	 * the algorithm picks up the node with the lowest degree in the graph
	 * as a starting point. This works for us, but there are many other
	 * heuristics out there!
	 */
	Queue* CuthillMcKee(Graph* G, GraphInt StartingNodeId );


#endif /* end of _RCM_H_ definition */

