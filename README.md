This is a sequential, naive implementation of the reverse Cuthill-McKee algorith
for sparse matrix bandwidth reduction.

Currently, no complex heuristic is implemented for selecting a periherial vertex,
instead, the lowest degree node in the graph is selected as starting node for the
graph traversal.