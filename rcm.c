#include "rcm.h"

GraphInt GetNodeWithLowestDegree(Graph *G, Queue *Q, char *isInR)
{
	GraphInt NodeWithMinDegreeId = 0;
	GraphInt val = (GraphInt)  INT_MAX;

	/* Iterates over the graph nodes searching for the node with lowest degree */
	for(GraphInt node=0; node < G->n; node++)
		if(GraphDegreeOfNode(G,node) < val && isInR[node] == '0'){
			NodeWithMinDegreeId = node;
			val = GraphDegreeOfNode(G,node);
		}

	return (NodeWithMinDegreeId);
};

Queue* CuthillMcKee(Graph* G, GraphInt StartingNodeId )
{

	Queue *Q = QueueInit();
	Queue *R = QueueInit();
	GraphInt* neighbors;
	char *isInR;
	GraphInt start;

	isInR = (char*) malloc (sizeof(char)*G->n);
	isInR = memset (isInR, '0', G->n);

	/* Decide the starting node for the algorithm */
	if ( StartingNodeId >= 0 )
		start = StartingNodeId;
	else
		start = GetNodeWithLowestDegree(G, R, isInR);
	
#if RCM_DEBUG
	fprintf(stderr, "%s: %d: Starting with node: %d\n", __FUNCTION__, __LINE__, start);
#endif

	/* Add the starting node to the queue before starting the RCM routine */
	QueueInsert(Q, start);
	
	/* RCM algorithm body */
	while(R->size < G->n){
		QueueValue_t actual = QueuePeek(Q);

		if(isInR[actual]=='0')
		{
			QueueInsert(R, actual);
			isInR[actual] = '1';
			neighbors = GetNeighbors(G, actual);
			
			for(int i=GraphDegreeOfNode(G, actual) -1; i >= 0; i--)
				if(isInR[neighbors[i]]=='0')
					QueueInsert(Q, neighbors[i]);

#if RCM_DEBUG
			printf("R:\n");
			QueuePrint(R);
			printf("Q:\n");
			QueuePrint(Q);
			getchar();
#endif
		}

		if(Q->size < 1) { QueueInsert(Q, GetNodeWithLowestDegree(G, R, isInR)); }
	}

	QueueFree(Q);
	return R;
}

