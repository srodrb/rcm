#include "rcm.h"


int main (int argc, char *argv[])
{
	int ku_prev, kl_prev; /* original upper and lower bandwidth values */
	int ku_rcm, kl_rcm;   /* bandwidth values after RCM ordering       */

	printf("Loading Matrix...\n");

	//Graph *T = LoadTestGraph();
	Graph *T = LoadFromBinary("TestMatrix/spike/s3rmt3m3.bin");
	
	fprintf(stderr, "Matrix dimension %d by %d \n", T->n, T->n);

	GraphComputeBandwidth(T, &ku_prev, &kl_prev);
	fprintf(stderr, "Bandwith Before CuthillMcKee: (%d,%d)\n", ku_prev, kl_prev);

	/*Compute the initial node taking one with the lower degree.*/
	fprintf(stderr, "Executing Cuthill-McKee...\n");
	Queue *R =  CuthillMcKee(T, RCM_NO_SEED );

	/* Apply the permutation to the graph */
	ChangeGraphIndex(QueueGetList(R), T);

	/* Compute the bandwidth after reordering */
	GraphComputeBandwidth( T, &ku_rcm, &kl_rcm);

	fprintf(stderr, "Bandwith After CuthillMcKee: (%d,%d) \n", ku_rcm, kl_rcm );
	
	/* Tidy up */
	GraphFree(T);

	return 0;
} 
